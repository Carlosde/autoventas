﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace Autoventas.Models
{
    public enum FormaPago
    {
        Tarjeta, Deposito 
    }
    public class Venta
    {
        [Key]
        public int idVenta {set; get; }
        [Display(Name = "Fecha")]
        [Required(ErrorMessage = "Este Campo es Obligatorio"), DataType(DataType.Date)]
        public DateTime fecha {get; set; }
        [Display(Name = "Forma de Pago")]
        [Required(ErrorMessage = "Este Campo es Obligatorio")]
        public FormaPago? forma { get; set; }
        [Display(Name = "Ganancia")]
        [Required(ErrorMessage = "Este Campo es Obligatorio")]
        public int ganancia { get; set; }

        public virtual Usuario usuario { set; get;}
        public virtual Vehiculo vehiculo {set; get;}
    }
}