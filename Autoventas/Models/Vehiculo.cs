﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.IO;

namespace Autoventas.Models
{
    public enum TipoVehiculo
    {
        Automovil, Motocicleta, PickUp, Autobus, Camioneta, Camion, Trailer
    }
    public enum Combustible
    {
        Gasolina, Diesel
    }
    public enum Color
    {
        Amarillo, Azul, Blanco, Cafe, Celeste,Corinto, Gris, Morado, Naranja, Negro, Rojo, Rosado, Verde
    }
    public enum Marca
    {
        Audi,Bajaj,BlueBird,BMW,CAT,Chevrolet,Dodge,Ducati,Ferrari,Fiat,Ford,GMC,HarleyDavison,Honda,Hyundai,International,ISUZU,Italika,Jeep,Kawasaki,KIA,
        Lamborghini,Mazda,MercedesBenz,Mitsubishi,Nissan,Paugeot,Porsche,Suzuki,Toyota,Volkswagen,Volvo,Yamaha
    }
    public class Vehiculo
    {
        [Key]
        public int idVehiculo { get; set; }
        [Display(Name = "Marca")]
        [Required(ErrorMessage = "Este Campo es Obligatorio"), DataType(DataType.Text)]
        public Marca? marca { get; set; }
        [Display(Name = "Modelo")]
        [Required(ErrorMessage = "Este Campo es Obligatorio"),DataType(DataType.Text)]
        public string modelo { get; set; }
        [Display(Name = "Año")]
        [Required(ErrorMessage = "Este Campo es Obligatorio"), DataType(DataType.Text)]
        [StringLength(4,MinimumLength=2, ErrorMessage = "El Año debe tener cuatro digitos, sin coma" )]
        public string año { get; set; }
        [Display(Name = "Color")]
        [Required(ErrorMessage = "Este Campo es Obligatorio")]
        public Color? color { get; set; }
        [Display(Name = "Precio de Compra (Q)")]
        [Required(ErrorMessage = "Este Campo es Obligatorio")]
        public int precioCompra { get; set; }
        [Display(Name = "Precio (Q)")]
        [Required(ErrorMessage = "Este Campo es Obligatorio")]
        public int precio { get; set; }
        public string estado { set; get; }
        [Display(Name = "Combustible")]
        [Required(ErrorMessage = "Este Campo es Obligatorio")]
        public Combustible? combustible { get; set; }
        [Display(Name = "Tipo de Vehiculo")]
        [Required(ErrorMessage = "Este Campo es Obligatorio")]
        public TipoVehiculo? tipo { get; set; }

        public virtual List<Archivo> archivos { set; get; }

        public virtual List<Venta> ventas { get; set; }

        public virtual List<Apartado> apartados { get; set; }
    }
}