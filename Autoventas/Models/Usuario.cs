﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Autoventas.Models
{
    public class Usuario
    {
        [Key]
        public int idUsuario { get; set; }
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "Este Campo es Obligatorio"), DataType(DataType.Text)]
        public string nombre { get; set; }
        [Display(Name = "Correo")]
        [Required(ErrorMessage = "Este Campo es Obligatorio"),DataType(DataType.EmailAddress)]
        public string correo { get; set; }
        [Display(Name = "Contraseña")]
        [Required(ErrorMessage = "Este Campo es Obligatorio"),DataType(DataType.Password)]
        public string contraseña { get; set; }
        [Display(Name = "Comparar Contraseña")]
        [Required(ErrorMessage = "Este Campo es Obligatorio"), DataType(DataType.Password)]
        [Compare("contraseña", ErrorMessage = "Las Contraseñas no Coinciden.")]
        public string compararContraseña { get; set; }
        
        public virtual Rol rol { get; set; }

        public virtual List<Venta> ventas { get; set; }

        public virtual List<Apartado> apartados { get; set; }
    }
}