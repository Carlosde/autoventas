﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Autoventas.Models
{
    public partial class DB_Autoventas:DbContext
    {
        public DB_Autoventas() : base("name=DB_Autoventas") { }
        public virtual DbSet<Rol> rol { set; get; }
        public virtual DbSet<Usuario> usuario { set; get; }
        public virtual DbSet<Vehiculo> vehiculo { set; get; }
        public virtual DbSet<Venta> venta { set; get; }
        public virtual DbSet<Apartado> apartado { set; get; }
        public virtual DbSet<Archivo> archivo { set; get; }
    }
}