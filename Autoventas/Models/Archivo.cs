﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Autoventas.Models
{
    public class Archivo
    {
        [Key]
        public int idArchivo { set; get; }
        public string nombre { set; get; }
        public String contentType { set; get; }
        public FileType tipo { set; get; }
        public byte[] contenido { set; get; }

        public int? idVehiculo { set; get; }
        public virtual Vehiculo vehiculo { set; get; }
    }
}