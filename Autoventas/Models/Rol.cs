﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Autoventas.Models
{
    public class Rol
    {
        [Key]
        public int idRol { get; set; }
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "Este Campo es Obligatorio")]
        public string nombre { get; set; }
        [Display(Name = "Descripcion")]
        [Required(ErrorMessage = "Este Campo es Obligatorio")]
        public string descripcion { get; set; }

        public virtual List<Usuario> usuarios { get; set; }
    }
}