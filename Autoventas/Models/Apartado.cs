﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Autoventas.Models
{
    public class Apartado
    {
        [Key]
        public int idApartado { set; get; }
        [Display(Name = "Fecha")]
        [Required(ErrorMessage = "Este Campo es Obligatorio"), DataType(DataType.Date)]
        public DateTime fecha { get; set; }
        public virtual Usuario usuario { set; get; }
        public virtual Vehiculo vehiculo { set; get; }
    }
}