namespace Autoventas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DB_Autoventas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Apartadoes",
                c => new
                    {
                        idApartado = c.Int(nullable: false, identity: true),
                        fecha = c.DateTime(nullable: false),
                        usuario_idUsuario = c.Int(),
                        vehiculo_idVehiculo = c.Int(),
                    })
                .PrimaryKey(t => t.idApartado)
                .ForeignKey("dbo.Usuarios", t => t.usuario_idUsuario)
                .ForeignKey("dbo.Vehiculoes", t => t.vehiculo_idVehiculo)
                .Index(t => t.usuario_idUsuario)
                .Index(t => t.vehiculo_idVehiculo);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        idUsuario = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false),
                        correo = c.String(nullable: false),
                        contraseña = c.String(nullable: false),
                        compararContraseña = c.String(nullable: false),
                        rol_idRol = c.Int(),
                    })
                .PrimaryKey(t => t.idUsuario)
                .ForeignKey("dbo.Rols", t => t.rol_idRol)
                .Index(t => t.rol_idRol);
            
            CreateTable(
                "dbo.Rols",
                c => new
                    {
                        idRol = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false),
                        descripcion = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.idRol);
            
            CreateTable(
                "dbo.Ventas",
                c => new
                    {
                        idVenta = c.Int(nullable: false, identity: true),
                        fecha = c.DateTime(nullable: false),
                        forma = c.Int(nullable: false),
                        ganancia = c.Int(nullable: false),
                        usuario_idUsuario = c.Int(),
                        vehiculo_idVehiculo = c.Int(),
                    })
                .PrimaryKey(t => t.idVenta)
                .ForeignKey("dbo.Usuarios", t => t.usuario_idUsuario)
                .ForeignKey("dbo.Vehiculoes", t => t.vehiculo_idVehiculo)
                .Index(t => t.usuario_idUsuario)
                .Index(t => t.vehiculo_idVehiculo);
            
            CreateTable(
                "dbo.Vehiculoes",
                c => new
                    {
                        idVehiculo = c.Int(nullable: false, identity: true),
                        marca = c.Int(nullable: false),
                        modelo = c.String(nullable: false),
                        año = c.String(nullable: false, maxLength: 4),
                        color = c.Int(nullable: false),
                        precioCompra = c.Int(nullable: false),
                        precio = c.Int(nullable: false),
                        estado = c.String(),
                        combustible = c.Int(nullable: false),
                        tipo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idVehiculo);
            
            CreateTable(
                "dbo.Archivoes",
                c => new
                    {
                        idArchivo = c.Int(nullable: false, identity: true),
                        nombre = c.String(),
                        contentType = c.String(),
                        tipo = c.Int(nullable: false),
                        contenido = c.Binary(),
                        idVehiculo = c.Int(),
                    })
                .PrimaryKey(t => t.idArchivo)
                .ForeignKey("dbo.Vehiculoes", t => t.idVehiculo)
                .Index(t => t.idVehiculo);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Ventas", "vehiculo_idVehiculo", "dbo.Vehiculoes");
            DropForeignKey("dbo.Archivoes", "idVehiculo", "dbo.Vehiculoes");
            DropForeignKey("dbo.Apartadoes", "vehiculo_idVehiculo", "dbo.Vehiculoes");
            DropForeignKey("dbo.Ventas", "usuario_idUsuario", "dbo.Usuarios");
            DropForeignKey("dbo.Usuarios", "rol_idRol", "dbo.Rols");
            DropForeignKey("dbo.Apartadoes", "usuario_idUsuario", "dbo.Usuarios");
            DropIndex("dbo.Archivoes", new[] { "idVehiculo" });
            DropIndex("dbo.Ventas", new[] { "vehiculo_idVehiculo" });
            DropIndex("dbo.Ventas", new[] { "usuario_idUsuario" });
            DropIndex("dbo.Usuarios", new[] { "rol_idRol" });
            DropIndex("dbo.Apartadoes", new[] { "vehiculo_idVehiculo" });
            DropIndex("dbo.Apartadoes", new[] { "usuario_idUsuario" });
            DropTable("dbo.Archivoes");
            DropTable("dbo.Vehiculoes");
            DropTable("dbo.Ventas");
            DropTable("dbo.Rols");
            DropTable("dbo.Usuarios");
            DropTable("dbo.Apartadoes");
        }
    }
}
