﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Autoventas.Models;
using System.Drawing;
using System.IO;
using System.Data.Entity.Infrastructure;

namespace Autoventas.Controllers
{
    public class VehiculoController : Controller
    {
        private DB_Autoventas db = new DB_Autoventas();

        // GET: Vehiculo
        public ActionResult Index(string search)
        {
            @ViewBag.separar = "|";
            var vehiculos = from v in db.vehiculo  
                           select v;
            if (!String.IsNullOrEmpty(search))
            {
                vehiculos = vehiculos.Where(v => v.marca.ToString().Contains(search)
                                            || v.modelo.Contains(search)
                                            || v.tipo.ToString().Contains(search)
                                            || v.combustible.ToString().Contains(search));
            }
            vehiculos = vehiculos.OrderBy(v => v.marca);
            
            return View(vehiculos.ToList());
        }

        // GET: Vehiculo/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.apartar = "|";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehiculo vehiculo = db.vehiculo.Find(id);

            if (vehiculo == null)
            {
                return HttpNotFound();
            }
            return View(vehiculo);
        }
        public ActionResult Venta()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Venta(int? id, Venta venta)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehiculo vehiculo = db.vehiculo.Find(id);
            Usuario usuario = db.usuario.Find(Session["idUsuario"]);
            DateTime fecha;
            fecha = DateTime.Now.Date;
            if (vehiculo == null || usuario==null)
            {
                return HttpNotFound();
            }
            venta.vehiculo = vehiculo;
            venta.usuario = usuario;
            venta.fecha = fecha;
            venta.ganancia = vehiculo.precio - vehiculo.precioCompra;
            vehiculo.estado = "Vendido";
            db.venta.Add(venta);
            db.SaveChanges();
            return Redireccion();
        }
        public ActionResult Apartado()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Apartado(int? id, Apartado apartado)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehiculo vehiculo = db.vehiculo.Find(id);
            Usuario usuario = db.usuario.Find(Session["idUsuario"]);
            DateTime fecha;
            fecha = DateTime.Now.Date;
            if (vehiculo == null || usuario == null)
            {
                return HttpNotFound();
            }
            apartado.vehiculo = vehiculo;
            apartado.usuario = usuario;
            apartado.fecha = fecha;
            vehiculo.estado = "Apartado";
            db.apartado.Add(apartado);
            db.SaveChanges();
            return RedirectToAction("../Apartado/Index");
        }
        public ActionResult Redireccion()
        {
            return RedirectToAction("../Venta/Index");
        }
        // GET: Vehiculo/Create
        public ActionResult Create()
        {
            List<int> años = new List<int>();
            for (int a = DateTime.Now.Year; a >= 1930; a--)
            {
                años.Add(a);
            }
            ViewBag.Año = new SelectList(años, "Año");
            return View();
        }

        // POST: Vehiculo/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idVehiculo,imagen,marca,modelo,año,color,precioCompra, precio,estado,combustible,tipo")] Vehiculo vehiculo, HttpPostedFileBase archivo)
        {
            if (ModelState.IsValid)
            {
                if (archivo != null && archivo.ContentLength>0)
                {
                    var imagen = new Archivo
                    {
                        nombre =  System.IO.Path.GetFileName(archivo.FileName),
                        tipo =FileType.Imagen,
                        contentType=archivo.ContentType
                    };
                    using (var reader = new System.IO.BinaryReader(archivo.InputStream))
                    {
                        imagen.contenido = reader.ReadBytes(archivo.ContentLength);
                    };
                    vehiculo.archivos = new List<Archivo> { imagen };
                }

                List<String> años = new List<String>();
                for (int a = DateTime.Now.Year; a > 1930; a--)
                {
                    años.Add(a.ToString());
                }

                ViewBag.Año = new SelectList(años, "", "", vehiculo.año);

                vehiculo.estado = "En Venta";
                db.vehiculo.Add(vehiculo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vehiculo);
        }

        // GET: Vehiculo/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehiculo vehiculo = db.vehiculo.Find(id);
            if (vehiculo == null)
            {
                return HttpNotFound();
            }
            List<int> años = new List<int>();
            for (int a = DateTime.Now.Year; a >= 1930; a--)
            {
                años.Add(a);
            }
            ViewBag.Año = new SelectList(años, "Año");
            return View(vehiculo);
        }

        // POST: Vehiculo/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName ("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int? id, HttpPostedFileBase archivo)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var vehiculo = db.vehiculo.Find(id);
            if(TryUpdateModel(vehiculo,"", new String[]{"idVehiculo,marca,modelo,año,color,precio,estado,combustible,tipo"} )){
                try{
                    if(archivo!=null && archivo.ContentLength>0){
                        if(vehiculo.archivos.Any(i => i.tipo==FileType.Imagen)){
                            db.archivo.Remove(vehiculo.archivos.First(i => i.tipo == FileType.Imagen));
                        }
                        var imagen = new Archivo
                        {
                            nombre = System.IO.Path.GetFileName(archivo.FileName),
                            tipo = FileType.Imagen,
                            contentType = archivo.ContentType
                        };
                        using(var reader= new System.IO.BinaryReader(archivo.InputStream))
                        {
                            imagen.contenido = reader.ReadBytes(archivo.ContentLength);
                        }
                        vehiculo.archivos = new List<Archivo> { imagen };
                    }
                    db.Entry(vehiculo).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }catch(RetryLimitExceededException ex){
                    ModelState.AddModelError("", "Unable to save changes. Try again.");
                }
            }
            List<String> años = new List<String>();
            for (int a = DateTime.Now.Year; a > 1930; a--)
            {
                años.Add(a.ToString());
            }

            ViewBag.Año = new SelectList(años, "", "", vehiculo.año);
            return View(vehiculo);
        }

        // GET: Vehiculo/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehiculo vehiculo = db.vehiculo.Find(id);

            if (vehiculo == null)
            {
                return HttpNotFound();
            }
            return View(vehiculo);
        }

        // POST: Vehiculo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, HttpPostedFileBase x)
        {
            var vehiculo = db.vehiculo.Find(id);

            Archivo archivo = db.archivo.FirstOrDefault(a => a.idVehiculo == id);
            if (archivo != null)
            {
                db.archivo.Remove(archivo);
            }

            db.vehiculo.Remove(vehiculo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Catalogo(string search)
        {
            ViewBag.apartar = "|";
            var vehiculos = from v in db.vehiculo
                            select v;
            if (!String.IsNullOrEmpty(search))
            {
                vehiculos = vehiculos.Where(v => v.marca.ToString().Contains(search)
                                            || v.modelo.Contains(search)
                                            || v.tipo.ToString().Contains(search)
                                            || v.combustible.ToString().Contains(search));
            }

            return View(vehiculos.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
