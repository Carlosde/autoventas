﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Autoventas.Models;

namespace Autoventas.Controllers
{
    public class ApartadoController : Controller
    {
        private DB_Autoventas db = new DB_Autoventas();

        // GET: Apartado
        public ActionResult Index(string search)
        {
            var reservas = from r in db.apartado
                            select r;
            if (!String.IsNullOrEmpty(search))
            {
                reservas = reservas.Where(r => r.vehiculo.marca.ToString().Contains(search));
            }
            reservas = reservas.OrderBy(r => r.vehiculo.marca);

            return View(reservas.ToList());
        }
        public ActionResult Venta()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Venta(int? id, Venta venta)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Apartado apartado = db.apartado.Find(id);
            Vehiculo vehiculo = apartado.vehiculo;
            DateTime fecha;
            fecha = DateTime.Now.Date;
            if (apartado == null)
            {
                return HttpNotFound();
            }
            venta.vehiculo = vehiculo;
            venta.usuario = apartado.usuario;
            venta.fecha = fecha;
            venta.ganancia = vehiculo.precio - vehiculo.precioCompra;
            vehiculo.estado = "Vendido";
            db.venta.Add(venta);
            db.apartado.Remove(apartado);
            db.SaveChanges();
            return RedirectToAction("../Venta/Index");
        }

        // GET: Apartado/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Apartado apartado = db.apartado.Find(id);
            if (apartado == null)
            {
                return HttpNotFound();
            }
            return View(apartado);
        }

        // GET: Apartado/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Apartado/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idApartado,fecha")] Apartado apartado)
        {
            if (ModelState.IsValid)
            {
                db.apartado.Add(apartado);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(apartado);
        }

        // GET: Apartado/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Apartado apartado = db.apartado.Find(id);
            if (apartado == null)
            {
                return HttpNotFound();
            }
            return View(apartado);
        }

        // POST: Apartado/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idApartado,fecha")] Apartado apartado)
        {
            if (ModelState.IsValid)
            {
                db.Entry(apartado).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(apartado);
        }

        // GET: Apartado/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Apartado apartado = db.apartado.Find(id);
            if (apartado == null)
            {
                return HttpNotFound();
            }
            return View(apartado);
        }

        // POST: Apartado/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Apartado apartado = db.apartado.Find(id);
            db.apartado.Remove(apartado);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
