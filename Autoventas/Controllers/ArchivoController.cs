﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Autoventas.Models;

namespace Autoventas.Controllers
{
    public class ArchivoController : Controller
    {
        public DB_Autoventas db = new DB_Autoventas();
        // GET: Archivo
        public ActionResult ObtenerArchivo(int id)
        {
            var imagen = db.archivo.Find(id);
            return File(imagen.contenido, imagen.contentType);
        }
    }
}