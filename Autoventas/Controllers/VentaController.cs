﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Autoventas.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace Autoventas.Controllers
{
    public class VentaController : Controller
    {
        private DB_Autoventas db = new DB_Autoventas();

        // GET: Venta
        public ActionResult Index(string search)
        {
            var ventas = from v in db.venta
                            select v;
            if (!String.IsNullOrEmpty(search))
            {
                ventas = ventas.Where(v => v.vehiculo.marca.ToString().Contains(search));
            }
            ventas = ventas.OrderBy(v => v.vehiculo.marca);

            return View(ventas.ToList());
        }

        // GET: Venta/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Venta venta = db.venta.Find(id);
            if (venta == null)
            {
                return HttpNotFound();
            }
            return View(venta);
        }
        // GET: Venta/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Venta venta = db.venta.Find(id);
            if (venta == null)
            {
                return HttpNotFound();
            }
            return View(venta);
        }

        // POST: Venta/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idVenta,fecha,forma")] Venta venta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(venta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(venta);
        }

        // GET: Venta/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Venta venta = db.venta.Find(id);
            if (venta == null)
            {
                return HttpNotFound();
            }
            return View(venta);
        }

        // POST: Venta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Venta venta = db.venta.Find(id);
            db.venta.Remove(venta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Reporte()
        {
            // Creamos el documento con el tamaño de página tradicional
            Document doc = new Document(PageSize.LETTER);
            String path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + " //reporte" + DateTime.Now.Day+"_"+DateTime.Now.Month+"_"+DateTime.Now.Day+"_"+DateTime.Now.Hour+"_"+DateTime.Now.Second+".pdf";
            // Indicamos donde vamos a guardar el documento
            PdfWriter writer = PdfWriter.GetInstance(doc,
                                        new FileStream(path, FileMode.Create));

            // Le colocamos el título y el autor
            // **Nota: Esto no será visible en el documento
            doc.AddTitle("Reporte");
            doc.AddCreator("Autoventas  Cars");

            // Abrimos el archivo
            doc.Open();

            // Creamos el tipo de Font que vamos utilizar
            iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

            // Escribimos el encabezamiento en el documento
            doc.Add(new Paragraph("Reporte de Ventas"));
            doc.Add(Chunk.NEWLINE);

            // Creamos una tabla que contendrá el nombre, apellido y país
            // de nuestros visitante.
            PdfPTable tblPrueba = new PdfPTable(7);
            tblPrueba.WidthPercentage = 100;

            // Configuramos el título de las columnas de la tabla
            PdfPCell clNombre = new PdfPCell(new Phrase("Nombre", _standardFont));
            clNombre.BorderWidth = 0;
            clNombre.BorderWidthBottom = 0.75f;

            PdfPCell clMarca = new PdfPCell(new Phrase("Marca", _standardFont));
            clMarca.BorderWidth = 0;
            clMarca.BorderWidthBottom = 0.75f;

            PdfPCell clModelo = new PdfPCell(new Phrase("Modelo", _standardFont));
            clModelo.BorderWidth = 0;
            clModelo.BorderWidthBottom = 0.75f;

            PdfPCell clCompra = new PdfPCell(new Phrase("Precio de Compra", _standardFont));
            clCompra.BorderWidth = 0;
            clCompra.BorderWidthBottom = 0.75f;

            PdfPCell clPrecio = new PdfPCell(new Phrase("Precio de Venta", _standardFont));
            clPrecio.BorderWidth = 0;
            clPrecio.BorderWidthBottom = 0.75f;

            PdfPCell clGanancia = new PdfPCell(new Phrase("Ganancia", _standardFont));
            clGanancia.BorderWidth = 0;
            clGanancia.BorderWidthBottom = 0.75f;

            PdfPCell clFecha = new PdfPCell(new Phrase("Fecha", _standardFont));
            clFecha.BorderWidth = 0;
            clFecha.BorderWidthBottom = 0.75f;
            // Añadimos las celdas a la tabla
            tblPrueba.AddCell(clNombre);
            tblPrueba.AddCell(clMarca);
            tblPrueba.AddCell(clModelo);
            tblPrueba.AddCell(clCompra);
            tblPrueba.AddCell(clPrecio);
            tblPrueba.AddCell(clGanancia);
            tblPrueba.AddCell(clFecha);

            // Llenamos la tabla con información
            
            foreach(var venta in db.venta)
            {
                clNombre = new PdfPCell(new Phrase(venta.usuario.nombre, _standardFont));
                clNombre.BorderWidth = 0;

                clMarca = new PdfPCell(new Phrase(venta.vehiculo.marca.ToString(), _standardFont));
                clMarca.BorderWidth = 0;

                clModelo = new PdfPCell(new Phrase(venta.vehiculo.modelo, _standardFont));
                clModelo.BorderWidth = 0;

                clCompra = new PdfPCell(new Phrase(venta.vehiculo.precioCompra.ToString(), _standardFont));
                clCompra.BorderWidth = 0;

                clPrecio = new PdfPCell(new Phrase(venta.vehiculo.precio.ToString(), _standardFont));
                clPrecio.BorderWidth = 0;

                clGanancia = new PdfPCell(new Phrase(venta.ganancia.ToString(), _standardFont));
                clGanancia.BorderWidth = 0;

                clFecha = new PdfPCell(new Phrase(venta.fecha.ToString(), _standardFont));
                clFecha.BorderWidth = 0;
            }

            // Añadimos las celdas a la tabla
            tblPrueba.AddCell(clNombre);
            tblPrueba.AddCell(clMarca);
            tblPrueba.AddCell(clModelo);
            tblPrueba.AddCell(clCompra);
            tblPrueba.AddCell(clPrecio);
            tblPrueba.AddCell(clGanancia);
            tblPrueba.AddCell(clFecha);

            // Finalmente, añadimos la tabla al documento PDF y cerramos el documento
            doc.Add(tblPrueba);

            doc.Close();
            writer.Close();

            return RedirectToAction("Index");
        }
    }
}
