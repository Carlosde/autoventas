﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Autoventas.Models;

namespace Autoventas.Controllers
{
    public class CuentaController : Controller
    {
        public DB_Autoventas db = new DB_Autoventas();

        // GET: Cuenta
        public ActionResult Login()
        {
            if(Session["idUsuario"]!=null){
                return RedirectToAction("../Home/Index");
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult Login(Usuario usuario)
        {
            var usr = db.usuario.FirstOrDefault(u => u.correo == usuario.correo && u.contraseña == usuario.contraseña);
            if(usr!=null){
                Session["nombreUsuario"] = usr.nombre;
                Session["idUsuario"] = usr.idUsuario;
                Session["idRol"] = usr.rol.idRol;
                return VerificarSesion();
            }
            else
            {
                ModelState.AddModelError("", "Correo o Contraseña Incorrectos... Verifique sus Credenciales");
            }
            return View();
        }
        // GET: Cuenta
        public ActionResult Registro()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Registro(Usuario usuario)
        {
            if(ModelState.IsValid ){
                Rol rol = db.rol.FirstOrDefault(r => r.idRol == 2);
                usuario.rol = rol;
                db.usuario.Add(usuario);
                db.SaveChanges();
            }
            return RedirectToAction("Login");
        }
        // GET: Cuenta
        public ActionResult ListarUsuarios(string search)
        {
            var usuarios = from u in db.usuario
                            select u;
            if (!String.IsNullOrEmpty(search))
            {
                usuarios = usuarios.Where(u => u.nombre.Contains(search));
            }
            usuarios = usuarios.OrderBy(v => v.nombre);
            return View(usuarios.ToList());
        }
        // GET: Cuenta
        public ActionResult CrearAdministrador()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CrearAdministrador(Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                Rol rol = db.rol.FirstOrDefault(r => r.idRol == 1);
                usuario.rol = rol;
                db.usuario.Add(usuario);
                db.SaveChanges();
                ViewBag.mensaje = "El Administrador " + usuario.nombre + " Fue Creado Correctamente";
            }
            return View();
        }
        // GET: Vehiculo/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = db.usuario.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // POST: Vehiculo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Usuario usuario = db.usuario.Find(id);
            db.usuario.Remove(usuario);
            db.SaveChanges();
            return RedirectToAction("ListarUsuarios");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult VerificarSesion()
        {
            if(Session["idUsuario"]!=null){
                return RedirectToAction("../Home/Index");
            }
            else
            {
                return RedirectToAction("Login");
            }
        }
        public ActionResult Logout()
        {
            Session.Remove("idUsuario");
            Session.Remove("nombreUsuario");
            Session.Remove("rol");
            return RedirectToAction("Login");
        }
        // GET: Venta/Details/5
        public ActionResult Details()
        {
            Usuario usuario = db.usuario.Find(Session["idUsuario"]);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }
        public ActionResult Edit()
        {
            Usuario usuario = db.usuario.Find(Session["idUsuario"]);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // POST: Venta/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(usuario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("../Home/Index");
            }
            return View(usuario);
        }
    }
}